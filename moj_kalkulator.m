   %1
   fprintf('Press enter to display Ex.1 \n')
   pause
   h = 6.62607015E-34 %Planck constat was taken from https://physics.nist.gov/cgi-bin/cuu/Value?h|search_for=universal_in!
   fprintf('h/(2*pi) =  %e \n', h/(2*pi))
   
   %2
   fprintf('\n Press enter to display next Ex.2 \n ')
   pause
   fprintf('sin(30/e)= %f \n', sind(30/exp(1))) %sind is sin where argument is degree
   
   %3
   fprintf('\n Press enter to display next Ex.3 \n')
   pause
   fprintf('0x00123d3 / (2,4551023) = %e \n', hex2dec('00123d3')/ (2.455e23)) %hex2dec it convert hexadecimal numbers to decimal
   
   %4
   fprintf('\n Press enter to display next Ex.4 \n')
   pause
   fprintf('sqrt(e-pi) = %fi \n', imag(sqrt(exp(1)-pi))) %imag function show imaginary part
   
   %5
   fprintf('\n Press enter to display next Ex.5 \n')
   pause
   fprintf('The tenth decimal place of pi is %d \n', mod(floor(pi*10e9),10)) %floor function it cuts everything after comma, mod funtion show remainder from division
   
   %6
   fprintf('\n Press enter to display next Ex.6 \n')
   pause
   fprintf('The number of days since my birthday(2019.07.19) uptil now - %d',datenum(date) - datenum(2019,07,19)) %datenum converts date to days since 1970, date function return today's date
   
   %7
   fprintf('\n Press enter to display next Ex.7 \n')
   pause
   Rz = 6378.137 % Radius of Earth was taken from https://nssdc.gsfc.nasa.gov/planetary/factsheet/earthfact.html
   fprintf('atan((exp(sqrt(7)/2-log(Rz/10^5)))/0xaabb) = %f',atan((exp(sqrt(7)/2-log(6378.137/1e5)))/hex2dec('aabb')))
   
   %8
   fprintf('\n Press enter to display next Ex.8 \n')
   pause
   Na = 6.02214076e23 % Avogadro constant was taken from https://physics.nist.gov/cgi-bin/cuu/Value?na|search_for=avogadro
   fprintf('Number of atoms in 1/5 micromol of ethanol is %e', 0.2 * 1e-6 * Na)
   
   %9
   fprintf('\n Press enter to display next Ex.9 \n')
   pause
   fprintf('Number of  %f', 2/9*1/101*1000)  
   
   
   